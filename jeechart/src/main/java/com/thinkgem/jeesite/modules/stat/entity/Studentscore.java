/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.thinkgem.jeesite.modules.stat.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.IdEntity;

/**
 * 学生成绩Entity
 * @author zhangfeng
 * @version 2014-09-18
 */
@Entity
@Table(name = "demo1_student_score")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Studentscore{
	
	private static final long serialVersionUID = 1L;
	private String id; 		// 编号
	private String stu_name; 	// 名称
	private String courses;
	private Integer score;

	public Studentscore() {
		super();
	}

	public Studentscore(String id){
		this();
		this.id = id;
	}
	
	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStu_name() {
		return stu_name;
	}

	public void setStu_name(String stu_name) {
		this.stu_name = stu_name;
	}

	public String getCourses() {
		return courses;
	}

	public void setCourses(String courses) {
		this.courses = courses;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
	
}


